# coding: utf-8

import math
import os
import os.path
import time
from datetime import timedelta

from probe import Probe


cfg = {
    "DEBUG": "True",
    "DBURI": "postgresql+pg8000://probe:probe@localhost/probe",
    "MAIL_SERVER": 'localhost',
    "MAIL_USER": 'user',
    "MAIL_PASSWORD": 'password',
    "MAIL_FROM": 'probe@triplet.gmbh',
    "API_TOKENS": '{"xxx": "someone"}',
    "VIEWS": '{"other": {"query": ["attribute", "type", "othertype"], "tokens": ["someone"]}}',
    "SECRET_KEY": 'xxx',
    'OMFGAUTH_PROVIDERS': '''[
        {
            "name": "Local Turret",
            "auth_url": "http://localhost:4000/authenticate/",
            "algorithm": "HS256",
            "secret": "EvenMoreSecret"
        }
    ]'''
}

probe = Probe(cfg)

app = probe.app


class PrinterPlugin:
    def __init__(self, config, session):
        pass

    def log(self, *args):
        print("logging", args)


@probe.registry.formatter('got_xxx')
def format_got_xxx(event):
    return ['we got an', 'XXX']


@probe.registry.formatter('end')
def format_end(event):
    return 'this is the end.'


@probe.registry.on_interval(timedelta(seconds=10))
def ping(api, event):
    """ all 10 seconds
    """
    api.trigger_event('message', {'content': 'Ping says Hi!'})


@probe.registry.on('message')
@probe.registry.with_plugin(PrinterPlugin)
def on_message(api, event, logger):
    logger.log("Here we go")


@probe.registry.on('message')
@probe.registry.with_plugin(PrinterPlugin)
def on_message_longrun(api, event, logger):
    state = api.get_state()
    api.update_state({})

    logger.log("Starting sleep")
    # time.sleep(5)
    api.trigger_event('done_sleeping', {})

    logger.log("Done.")


@probe.registry.on('got_xxx')
def fail_around(api, event):
    """ Produce an error """
    time.sleep(10)
    1 / 0


@probe.registry.on('got_xxx')
def count_xxx(api, event):
    state = api.get_state()
    state['count'] = count = state.get('count', 0) + 1
    api.update_state(state)
    api.trigger_event('count_xxx', {"count": count})


@probe.registry.on('webhook:xxx')
def rephrase_xxx(api, event):
    api.trigger_event('got_xxx', {})


@probe.registry.on('webhook:fail')
def webhook_fail(api, event):
    raise Exception("omfg!")


@probe.registry.on('got_xxx')
def got_xxx(api, event):
    api.trigger_event('end', {})
    api.trigger_event('end', {})


@probe.registry.on_missing_since('got_xxx', timedelta(seconds=10))
def notify_missing(api, event):
    api.trigger_event('notify_missing', {})



class PerfectSquareReport:
    name = 'perfect_square'
    title = 'Perfect Square'
    event_types = ['count_xxx']

    def initial(self):
        return {'value': 0}

    def reduce(self, data, event):
        count = event.data['count']

        if math.sqrt(count) == int(math.sqrt(count)):
            return {
                'value': max(count, data['value'])
            }
        else:
            return data

    def postprocess(self, data):
        return [
            {
                'title': 'Alpha',
                'values': [
                    {
                        'content': 'Hello',
                        'level': 'red'
                    }, {
                        'content': 'World',
                        'level': 'green'
                    }
                ]
            },
            {
                'title': 'Bravo',
                'values': [
                    {
                        'content': 'Hello',
                        'level': 'red'
                    }, {
                        'content': 'World',
                        'level': 'green'
                    }
                ]
            }
        ]


probe.registry.add_report(PerfectSquareReport())
