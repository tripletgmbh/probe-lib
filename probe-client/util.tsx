
export function concatAll<T>(lists: T[][]): T[] {
    return lists.reduce((acc, item) => [...acc, ...item], [])
}
