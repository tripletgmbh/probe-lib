import * as React from 'react';
import { FC } from 'react';

import { useParams } from 'react-router-dom';

import * as api from '../api';
import '../styles/ReportPage.scss';

import { Header, Main, useObservableFactory } from 'botparade';

import { Avatar } from './Avatar';



export const ReportPage: FC<{}> = () => {

    const params = useParams<{ report: string }>();
    const report = useObservableFactory(
                            () => api.fetchReport(params.report),
                            null,
                            [ params.report ]);

    return (
        <section className="ReportPage">
            <Header>
                <Avatar />
                <h1>{ report && report.title }</h1>
            </Header>
            <Main>
              { report &&
                <table className="Report">
                  { report.data.map((row, index) =>
                    <tr key={ index }>
                      <th>{ row.title }</th>
                      { row.values.map((value, index) =>
                        <td
                          key={ index }
                          className={ value.level || '' }
                        >
                          { value.content }
                        </td>
                      )}
                    </tr>
                  ) }
                </table>
              }
            </Main>
        </section>
    );
}
