import * as React from 'react';
import { FC } from 'react';

import * as api from '../api';
import { concatAll } from '../util';
import '../styles/Monitor.scss';

import { Header, Main, usePaginator } from 'botparade';

import { Avatar } from './Avatar';
import { Monitor } from './Monitor';


export const MonitorPage: FC<{}> = () => {
    const [ pages ] = usePaginator(api.fetchMonitorPage, () => ({}), []);
    return (
        <section className="MonitorPage">
            <Header>
                <Avatar />
                <h1>Monitors</h1>
            </Header>
            <Main>
                { pages && concatAll(pages).map(monitor =>
                    <Monitor
                        key={ monitor.name }
                        monitor={ monitor }
                    />
                ) }
            </Main>
        </section>
    );
}
