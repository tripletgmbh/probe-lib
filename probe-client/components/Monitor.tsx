import * as React from 'react';
import { FC, Fragment } from 'react';

import * as api from '../api';
import '../styles/Monitor.scss';

import { Panel, CodeBlock } from 'botparade';


export const Monitor: FC<{ monitor: api.Monitor }> = ({ monitor }) => (
    <Panel
        className="Monitor"
        key={ monitor.name }
        title={ monitor.name }
        content={
            <Fragment>
                <div className="Monitor-summary">
                    { monitor.documentation
                        && <p>{ monitor.documentation }</p>
                    }

                    { monitor.triggered_by.method === 'on' &&
                        <p>Triggered by <em>{ monitor.triggered_by.type }</em></p>
                    }

                    { monitor.triggered_by.method === 'on_missing' &&
                        <p>Triggered when <em>{ monitor.triggered_by.type }</em> is missing</p>
                    }
                </div>
                <CodeBlock
                    code={ JSON.stringify(monitor.state, null, 2) }
                    validator={ validate }
                    onChange={ state => api.updateMonitor(monitor, JSON.parse(state)) }
                />
            </Fragment>
        }
        expanded={ true }
    />
)


function validate(text: string): boolean {
    try {
        const result = JSON.parse(text);
        return typeof result === 'object';
    }
    catch (e) {
        return false;
    }
}
