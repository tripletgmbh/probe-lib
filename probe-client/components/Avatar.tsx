import * as React from 'react';
import { FC } from 'react';

import { NavLink } from 'react-router-dom';
import { Avatar as BaseAvatar, useObservableFactory } from 'botparade';

import * as api from '../api';

import '../styles/Avatar.scss';


const avatarUrl = '/images/avatar.png';


export const Avatar: FC<{}> = () => {

  const reports = useObservableFactory(api.fetchReports, [], []);

  return (
    <BaseAvatar
        portrait={
            <img
                className="ProbeAvatarImage"
                src={ avatarUrl }
            />
        }
        menu={
            <ul className="ProbeAvatarMenu">
                <li><NavLink to="/search">Events</NavLink></li>
                <li><NavLink to="/monitors">Monitors</NavLink></li>
                { reports.map(report =>
                  <li key={ report.name }>
                    <NavLink to={ `/reports/${ report.name }` }>{ report.title }</NavLink>
                  </li>
                ) }
            </ul>
        }
    />
  )
}
