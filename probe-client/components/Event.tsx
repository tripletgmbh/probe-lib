import * as React from 'react';
import { Fragment, FC } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faAngleUp,
    faAngleDown,
    faSpinner } from '@fortawesome/free-solid-svg-icons';

import { ProbeEvent } from '../api';
import '../styles/Event.scss';

import { CodeBlock, Panel, Time } from 'botparade';


interface EventProps {
    event: ProbeEvent
    onToggleEvent: (collapsed: boolean) => any
    collapsed?: boolean,
}


export const Event: FC<EventProps> = ({event, collapsed, onToggleEvent}) => (
    <Panel
        className="Event"
        title={
            <Fragment>
                <span className="Event-id">{ event.id }</span> {' '}
                <span className="Event-description">
                    { event.description.map((text: string, index: number) =>
                        (index % 2)
                            ? <em key={index}>{text}{' '}</em>
                            : <span key={index}>{text}{' '}</span>
                    ) }
                </span>
            </Fragment>
        }
        titleRight={
            <Fragment>
              { event.has_queued_executions &&
                <Fragment>
                  <FontAwesomeIcon
                      fixedWidth={ true }
                      icon={ faSpinner }
                      spin={ true }
                  />
                  { ' ' }
                </Fragment>
              }
              <Time
                  className="Event-time"
                  datetime={ event.datetime }
              />
            </Fragment>
        }
        icons={
            <button onClick={ () => onToggleEvent(!collapsed) }>
              <FontAwesomeIcon
                  fixedWidth={ true }
                  icon={ !collapsed ? faAngleUp : faAngleDown }
              />
            </button>
        }
        content={
            <CodeBlock code={ JSON.stringify(event.body, null, 2) } />
        }
        expanded={ !collapsed }
    />
);
