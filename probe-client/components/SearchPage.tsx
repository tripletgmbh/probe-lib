import * as React from 'react';
import { FC } from 'react';

import { RouteComponentProps } from 'react-router-dom'

import { fetchEventPage } from '../api';
import { concatAll } from '../util';
import '../styles/SearchPage.scss';

import { Avatar } from './Avatar';
import { Event } from './Event';

import { Header, Main, useUrlParameter, usePaginator } from 'botparade';

import { parse as _parse } from '../searchgrammar.pegjs'


type Props = RouteComponentProps<{}>;


function parse(text: string): any | null {
    try {
        return _parse(text.trim());
    }
    catch {
        return null
    }
}


export const SearchPage: FC<Props> = ({ history }) => {

    const [ q, setQ ] = useUrlParameter(history, 'q');
    const [ expandedId, setExpandedId ] = useUrlParameter(history, 'expanded');

    const query = parse(q || '');

    const [ pages, loadPage ] = usePaginator(
                                        fetchEventPage,
                                        () => ({query: query || ['all']}),
                                        [ q || '' ]);

    return (
        <section className="SearchPage">
            <Header>
                <Avatar />
                <input
                    type="text"
                    className={ `SearchPage-filter ${ query === null ? 'error' : '' }` }
                    autoFocus={ true }
                    value={ q || '' }
                    onChange={ evt => { setQ(evt.target.value); } }
                />
            </Header>
            <Main>
                { pages && concatAll(pages).map(event =>
                    <Event
                        key={ event.id }
                        event={ event }
                        collapsed={ expandedId !== event.id.toString() }
                        onToggleEvent={ collapsed => {
                            setExpandedId(collapsed ? undefined : event.id);
                        } }
                    />
                ) }
                <button onClick={ loadPage }>Load more</button>
            </Main>
        </section>
    );
}
