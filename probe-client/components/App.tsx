import * as React from 'react';
import { Fragment, FC } from 'react';
import { HashRouter, Route, RouteProps, Redirect } from 'react-router-dom'

import { useObservable } from 'botparade';

import { SearchPage } from './SearchPage';
import { MonitorPage } from './MonitorPage';
import { LoginPage } from './LoginPage';
import { ReportPage } from './ReportPage';

import { isLoggedIn } from '../api';


const PrivateRoute: FC<RouteProps> = props => {

  const loggedIn = useObservable(isLoggedIn);

  if (loggedIn === null) {
    return null;
  }

  return (loggedIn
    ? <Route { ...props } />
    : <Route
          { ...props }
          render={ () => <Redirect to={ `/login/` } /> }
          component={ undefined }
      />)
};


export const App: FC<{}> = () => (
    <HashRouter>
        <Fragment>
            <PrivateRoute
                exact
                path="/"
                render={ () => <Redirect to="/search/"/> }
            />
            <Route
                exact
                path="/login"
                component={ LoginPage }
            />
            <PrivateRoute
                path="/search/"
                component={ SearchPage }
            />
            <PrivateRoute
                path="/monitors/"
                component={ MonitorPage }
            />
            <PrivateRoute
                path="/reports/:report/"
                component={ ReportPage }
            />
        </Fragment>
    </HashRouter>
);
