import * as React from 'react';
import { FC, useState } from 'react';
import { useHistory } from 'react-router-dom';

import {
    Panel,
    PanelHeader,
    PanelFreetext,
    Header,
    Main,
    useObservableFactory } from 'botparade';

import { Avatar } from './Avatar';

import * as api from '../api';


export const LoginPage: FC<{}> = () => {

  const history = useHistory();
  const [ token, setToken ] = useState('');

  const valid = token.trim().length > 0;

  const providers = useObservableFactory(api.fetchOmfgauthProviders, [], []);

  function handleLogin(evt: React.FormEvent<HTMLFormElement>) {
    evt.preventDefault();
    api.login(token).subscribe(success => {
      if (success) {
        history.push('/');
      }
    });
  }

  return (
    <section className="LoginPage">
      <Header>
        <Avatar />
        <h1>Login</h1>
      </Header>
      <Main>
        <Panel expanded={ true }>
          <PanelHeader>
            Login via API Token
          </PanelHeader>
          <PanelFreetext>
            <form onSubmit={ handleLogin  }>
                <input
                    type="text"
                    className="Input"
                    placeholder="Api Token"
                    onChange={ evt => setToken(evt.target.value) }
                    value={ token }
                />
                {' '}
                <button
                    className="Button Primary"
                    type="submit"
                    disabled={ !valid }
                >
                  Login
                </button>
            </form>
          </PanelFreetext>
        </Panel>

        { (providers.length > 0) &&
          <Panel expanded={ true }>
            <PanelHeader>
              Login via OMFG-Auth
            </PanelHeader>
            <PanelFreetext>
            <table>
              { providers.map((p, index) =>
                <tr key={ index }>
                  <th>
                    { p.name }
                  </th>
                  <td>
                    <form method="POST" action={ p.url }>
                      <button className="Button Primary">Login</button>
                    </form>
                  </td>
                </tr>
              ) }
            </table>
            </PanelFreetext>
          </Panel>
        }
      </Main>
    </section>
  );
}
