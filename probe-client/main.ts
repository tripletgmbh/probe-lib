import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { App } from './components/App';

import 'botparade/dist/styles/reset.scss';
import 'botparade/dist/styles/FormElements.scss';
import 'botparade/dist/styles/Utility.scss';

import './styles/font.scss';

ReactDOM.render(
    React.createElement(App, {}),
    document.querySelector('#application')
);
