
type Node = ['all'] | ['fuzzy', ...Node] | ['and', ...Node]


declare module Peg {
    export function parse(input: string): Node
}

export = Peg