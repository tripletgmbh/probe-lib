import { Observable, BehaviorSubject } from 'rxjs';
import { ajax } from 'rxjs/ajax'
import { map, tap } from 'rxjs/operators'


export interface ProbeEvent {
    id: string,
    type: string,
    description: string[],
    datetime: Date,
    body: any,
    has_queued_executions: boolean,
}


interface EventResponse {
    events: ProbeEvent[]
};


export interface Monitor {
    name: string,
    documentation: string | null,
    triggered_by: {
        method: 'on' | 'on_missing',
        type: string,
    },
    state: any,
}


interface MonitorResponse {
    monitors: Monitor[]
}


interface Cursor {
    query: any,
    until?: string,
}


interface Report {
    title: string,
    data: {
        title: string
        values: {
            content: string,
            level?: 'red' | 'green' | 'yellow'
        }[]
    }[],
}


export function fetchEventPage(cursor: Cursor) {
    const url = '/api/events/'
            + `?query=${ encodeURIComponent(JSON.stringify(cursor.query)) }`
            + `&until=${cursor.until || ''}`;

    return ajax.getJSON<EventResponse>(url).pipe(
        map(r => r.events.map(evt => ({
            ...evt,
            datetime: new Date(evt.datetime)
        }))),
        map(events => ({
            page: events,
            next: (events.length === 0)
                    ? null
                    : {query: cursor.query, until: events[events.length - 1].id}
        }))
    )
}


export function fetchMonitorPage(_cursor: {}) {
    return ajax.getJSON<MonitorResponse>('/api/monitors/').pipe(
        map(r => ({
            page: r.monitors,
            next: null,
        }))
    );
}


export function updateMonitor(monitor: Monitor, state: any) {
    ajax.patch(
        `/api/monitors/${ monitor.name }/`,
        JSON.stringify({state}),
        {'Content-Type': 'application/json'}
    ).subscribe(_ => {
        console.log('update monitors');
    })
}


export function fetchReport(report: string): Observable<Report> {
    return ajax.getJSON(`/api/reports/${ report }/`);
}


export function fetchReports(): Observable<{name: string, title: string}[]> {
  return ajax
    .getJSON<{ reports: { name: string, title: string }[] }>(`/api/reports/`)
    .pipe(map(r => r.reports));
}


export function login(api_token: string): Observable<boolean> {
    return ajax
      .post<{ success: boolean }>(
          '/api/login/',
          JSON.stringify({ api_token }),
          {'Content-Type': 'application/json'})
      .pipe(
        tap(r => {
          if (r.response.success) {
            currentTokenStream.next(api_token);
          }
        }),
        map(r => r.response.success))
}


interface OmfgAuthProvider {
    name: string,
    url: string,
}


export function fetchOmfgauthProviders(): Observable<OmfgAuthProvider[]> {
    return ajax
        .getJSON<{items: OmfgAuthProvider[]}>('/api/omfgauthproviders/')
        .pipe(map(response => response.items));
}


declare const currentToken: string | null;

const currentTokenStream = new BehaviorSubject(currentToken);

export const isLoggedIn = currentTokenStream.pipe(map(token => token !== null));
