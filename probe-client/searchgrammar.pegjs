    Expression
    = Conjunction
    / All

All
    = ''
    { return ['all'] }

Conjunction
    = head:Term tail:( _ Term ) *
    { 
        return tail.length == 0
            ? head
            : ['and', head, ...tail.map(([_, t]) => t)];
    }

Term
    = AttributeTerm
    / ValueTerm

AttributeTerm
    = att:Attribute ':' val:Value
    { return ['attribute', att, val] }
    
ValueTerm
    = val:Value
    { return ['fuzzy', val] }

Attribute
    = [a-zA-Z0-9_-]+
    { return text() }

Value
    = QuotedValue
    / UnquotedValue

QuotedValue
    = ('"' [^"]* '"' / "'" [^']* "'")
    { return text().slice(1, -1) }

UnquotedValue
    = [^ ]+
    { return text() }

_ = ' '+