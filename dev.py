#!/usr/bin/env python
import json
import sys
import subprocess
import shutil
import os
import os.path
from datetime import datetime


def abspath(*args):
    return os.path.abspath(os.path.join(os.path.dirname(__file__), *args))


def command_venv():
    """ initializes the virtualenv
    """
    if os.path.exists('venv'):
        return

    shell(['virtualenv', '-p', 'python3', 'venv'])
    shell(['pip', 'install', '--editable', '.'])
    shell(['pip', 'install', 'pg8000'])
    shell(['pip', 'install', 'pycodestyle'])


def command_node_modules():
    """ initializes node modules
    """
    if os.path.exists('node_modules'):
        return
    shell(['npm', 'install'])


def command_flask(*args):
    """ run flask cli
    """
    command_venv()
    shell(['flask'] + list(args), {
        "FLASK_DEBUG": "1",
        "FLASK_APP": "testapp:app",
    })


def command_build():
    """ builds the application, so that it can be published
    """
    with open(abspath('probe', 'buildinfo.py'), 'w') as fp:
        fp.write('version = %r\n' % datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))

    command_venv()
    command_test()
    command_node_modules()
    shell(['webpack', '--mode', 'production'])
    shell(['python', 'setup.py', 'sdist'])


def command_webpack(*args):
    """ runs webpack to generate scripts and styles
    """
    command_node_modules()
    shell(['webpack'] + list(args))


def command_exec(*args):
    """ runs a shell command, with the path set to include venv and node_modules
    """
    command_venv()
    command_node_modules()
    shell(args)


def command_clean():
    """ cleans the project dir
    """
    for name in ['venv', 'node_modules', 'probe_lib.egg-info']:
        if os.path.exists(name):
            shutil.rmtree(name)


def command_test():
    """ runs the tests
    """
    command_venv()

    ignore = ",".join([
        # 'comparison to None should be 'if cond is None', totally ok for sqlalchemy
        "E711",

        # too hard to get right, also, probably in some instances not satisfiable
        "E126",
        "E127",
        "E128",

        # 'line break before operator'.
        # very funny, since W504 forbids line break after.
        # like, what am i supposed to do, bitch?
        "W503",

        # 'do not assign a lambda expression, use a def'
        # counterpoint:
        #
        # def whatever(funcs):
        #    for func in funcs:
        #        if not func:
        #           func = lambda x: x
        "E731",

        # do not use bare 'except'
        # there are valid cases where we want to catch every error, but do
        # nothing with the error itself.
        "E722",
    ])

    shell(['pycodestyle', 'probe', '--ignore={}'.format(ignore)])

    disable = ",".join([
        # i dont care for conventions, aside pep8.
        # the goal is to avoid errors, not bitch about minutiae
        "C",

        # and i don't care for refactoring tips either
        "R",

        # unused argument, but e.g. 'current_user' is not always used, even if
        # the decorator is needed.
        "W0613",

        # Redefining built-in, but e.g. function arguments may have the name
        # filter due to styx interface definition
        "W0622",

        # No exception type(s) specified (bare-except)
        # see above: it's fine.
        "W0702",

        # Method should have "self" as first argument (no-self-argument)
        # not for sqlalchemys @declared_attr, which are basically classmethods
        "E0213",

        # Catching too general exception Exception (broad-except)
        # sometimes, we really want to check if anything went wrong at all
        "W0703",

        # Access to a protected member _reports of a client class (protected-access)
        # in this project _x is more like "module visible", not "private"
        "W0212",

#        "W0143",  # compare with a callable, totally ok for sqlalchemy
#        "W1505",  # we really like our assert_, thanks.
    ])

    shell(['pylint', 'probe',
                        '--disable={}'.format(disable),
#                        '--load-plugins=pylintplugins',
                        '--ignored-classes=SQLAlchemy,scoped_session'])

    # shell(['pytest', '-vv'] + list(args), default_config)
    #shell(['python', '-m', 'unittest', 'test'])


def command_help():
    """ displays the help message
    """
    print("usage: build <command> [<args>...]")
    print()
    print("commands:")
    width = max(len(name) for name in commands.keys())
    for name, func in commands.items():
        print("  %s - %s" % (name.ljust(width), (func.__doc__ or '').strip()))


class ShellError(RuntimeError):
    pass


def shell(args, env={}):
    allenv = {}
    allenv.update(os.environ)
    allenv.update(env)

    allenv.update({
        'PYTHONPATH': os.pathsep.join([
            abspath(),
            os.environ.get('PYTHONPATH', '')
        ]),
        'PATH': os.pathsep.join([
                    abspath('venv', 'Scripts'),
                    abspath('venv', 'bin'),
                    abspath('node_modules', '.bin'),
                ] + [os.environ['PATH']])
    })
    try:
        subprocess.check_call(' '.join(args), env=allenv, shell=True)
    except Exception as exc:
        raise ShellError(exc)


commands = dict((name[len('command_'):], func)
                for name, func in globals().items()
                if name.startswith('command_'))


def main(args):
    try:
        if not args or args[0] not in commands:
            command_help()
        else:
            commands[args[0]](*args[1:])
    except ShellError as err:
        print(err)
        sys.exit(1)


if __name__ == '__main__':
    main(sys.argv[1:])
