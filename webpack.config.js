const path = require('path');
const webpack = require('webpack');

// const Visualizer = require('webpack-visualizer-plugin');


module.exports = env => ({
    entry: "./probe-client/main.ts",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, 'probe/static')
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "ts-loader"
            }, {
                test: /\.scss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader",
                ]
            }, {
                test: /\.pegjs/,
                use: [
                    'pegjs-loader',
                ]
            }, {
              test: /\.(png|ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
              use: [{
                  loader: 'file-loader',
                  options: {
                      name: '[name].[ext]',
                      publicPath: '/static/'
                  }
              }]
            },
        ]
    },
    optimization: {
        minimize: (env && env.production),
    },
    plugins: [
  //      new Visualizer(),
        ...(env && env.production
              ? [
                  new webpack.DefinePlugin({
                      'process.env.NODE_ENV': JSON.stringify('production')
                  })
                ]
              : [])
    ]
})
