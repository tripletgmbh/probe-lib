# coding: utf-8
import sys
import traceback
import time
from datetime import datetime

from .model import (Event, MonitorState, MissingSinceTriggerState,
                    IntervalState, QueuedExecution, db, commit)


class Api(object):

    def __init__(self, config, monitor_name):
        self.config = config
        self.monitor_name = monitor_name
        self.enqueued_events = []

    def trigger_event(self, type, body):
        self.enqueued_events.append((type, body))

    def get_config(self, prefix=""):
        return dict((key[len(prefix):], value)
                    for key, value
                    in self.config.items()
                    if key.startswith(prefix))

    def get_state(self):
        return MonitorState.get_for_monitor(self.monitor_name)

    def update_state(self, state):
        MonitorState.set_for_monitor(self.monitor_name, state)


class Registry(object):

    def __init__(self):
        self._formatters = {}
        self._on = {}
        self._on_missing_since = []
        self._on_interval = []
        self._reports = []

    def formatter(self, type):
        def decorator(func):
            self._formatters[type] = func
            return func
        return decorator

    def with_plugin(self, *plugins):
        def decorator(func):
            if not hasattr(func, '_probe_plugins'):
                func._probe_plugins = []
            func._probe_plugins.extend(plugins)
            return func
        return decorator

    def on(self, type):
        def decorator(func):
            name = '%s.%s' % (func.__module__, func.__name__)
            self._on.setdefault(type, []).append((name, func))
            return func
        return decorator

    def on_missing_since(self, type, duration):
        def decorator(func):
            name = '%s.%s' % (func.__module__, func.__name__)
            self._on_missing_since.append((type, duration, name, func))
            return func
        return decorator

    def on_interval(self, duration):
        def decorator(func):
            name = '%s.%s' % (func.__module__, func.__name__)
            self._on_interval.append((duration, name, func))
            return func
        return decorator

    def get_monitor(self, name):
        for lst in self._on.values():
            for n, monitor in lst:
                if name == n:
                    return monitor
        raise Exception("monitor %r not found" % name)

    def get_monitors(self, type):
        return [(name, monitor)
                    for name, monitor
                    in self._on.get(type, [])]

    def get_all_monitors(self):
        for type, lst in self._on.items():
            for name, func in lst:
                yield name, func.__doc__, 'on', type
        for type, _, name, func in self._on_missing_since:
            yield name, func.__doc__, 'on_missing', type

    def get_format(self, type, body):
        func = self._formatters.get(type, lambda _: type)
        result = func(body)
        return [result] if isinstance(result, str) else result

    def get_watched_types(self):
        return frozenset(type for type, _, _, _ in self._on_missing_since)

    def get_report(self, name):
        return next((r for r in self._reports if r.name == name), None)

    def add_report(self, report):
        self._reports.append(report)


class Report:
    name = None
    title = None
    event_types = []
    event_amount = 300

    def initial(self):
        return {}

    def reduce(self, data, event):
        return data

    def postprocess(self, data):
        return data


class Executor(object):
    def __init__(self, registry, config):
        self.config = config
        self.registry = registry

    def _execute_monitor(self, name, monitor, data):
        api = Api(self.config, name)
        try:
            plugin_args = [plugin(self.config, db.session)
                            for plugin
                            in getattr(monitor, '_probe_plugins', [])]
            monitor(api, data, *plugin_args)
        except Exception as exc:
            _, _, tb = sys.exc_info()
            enqueue(self.registry, "error", {
                "exception": type(exc).__name__,
                "message": str(exc),
                "stack": [tuple(frame) for frame in traceback.extract_tb(tb)]
            })
        else:
            for event_type, body in api.enqueued_events:
                enqueue(self.registry, event_type, body)

    def dispatch_execution(self, ex):
        monitor = self.registry.get_monitor(ex.monitor)
        ex.delete()
        commit()
        self._execute_monitor(ex.monitor, monitor, ex.event.data)
        commit()

    def dispatch_missing(self):
        types = self.registry.get_watched_types()

        last_executions = dict(Event.get_last_executions(types))
        now = datetime.now()

        last_triggered_for = dict((m.monitor, m.last_triggered)
                                for m in MissingSinceTriggerState.get_all())

        for type, duration, name, monitor in self.registry._on_missing_since:
            date = last_executions.get(type, datetime.min)
            if date + duration < now and date != last_triggered_for.get(name):
                self._execute_monitor(name, monitor, {})
                MissingSinceTriggerState.set_for_monitor(name, date)
                commit()

    def dispatch_interval(self):
        now = datetime.now()
        last = {i.monitor: i.last_triggered for i in IntervalState.get_all()}

        for duration, name, monitor in self.registry._on_interval:
            if last.get(name, datetime.min) + duration < now:
                self._execute_monitor(name, monitor, {})
                IntervalState.set_for_monitor(name, now)
                commit()


def enqueue(registry, type, data):
    event = Event(type=type, data=data)
    for name, _ in registry.get_monitors(type):
        QueuedExecution(name, event)


def evaluate_report(registry, name):
    report = registry.get_report(name)
    events = Event.get_recent_by_types(report.event_types, report.event_amount)

    data = report.initial()
    for event in list(events)[::-1]:
        data = report.reduce(data, event)

    data = report.postprocess(data)

    return {
        'title': report.title,
        'data': data,
    }


def work(registry, config):
    print("working...")
    executor = Executor(registry, config)

    while True:
        executor.dispatch_missing()
        executor.dispatch_interval()

        for ex in QueuedExecution.get_all():
            print("dispatching Event {} to {}".format(
                                                ex.event.id, ex.monitor))
            executor.dispatch_execution(ex)

        time.sleep(0.5)
