# coding: utf-8

import json

import click
from flask import Blueprint, current_app

from . import model
from . import monitor


app = Blueprint('cli', __name__, cli_group=None)


@app.cli.command()
def work():
    """ starts the worker
    """
    monitor.work(current_app.registry, current_app.config)


@app.cli.command()
@click.argument('type')
@click.argument('data')
def trigger(type, data):
    """ triggers an event
    """
    monitor.enqueue(current_app.registry, type, json.loads(data))
    model.commit()
