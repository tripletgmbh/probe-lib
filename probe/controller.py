# coding: utf-8
import functools
import json
import jwt
from urllib.parse import urlparse

from flask import (Blueprint, render_template, request, session,
                    url_for, jsonify, abort, current_app, redirect)

from .buildinfo import version
from .model import Event, MonitorState, commit
from .monitor import enqueue, evaluate_report


app = Blueprint('controller', __name__, template_folder='templates')


def current_token():
    tokens = current_app.api_tokens
    return (session.get("token")
            or tokens.get(request.args.get('api_token'))
            or tokens.get(request.args.get('access_token')))


def auth_required(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        token = current_token()
        user = session.get('user', None)
        if token or user:
            return func(*args, **kwargs)
        else:
            return abort(401)
    return wrapper


@app.route('/')
def show_app():
    return render_template('app.html',
                                token=current_token() or session.get('user'),
                                version=version)


@app.route('/api/omfgauthproviders/')
def api_omfgauthproviders():
    return jsonify({
        "items": [
            {
                "name": provider['name'],
                "url": url_for('.omfgauth_outgoing', provider_id=provider_id),
            }
            for provider_id, provider
            in enumerate(current_app.omfgauth_providers)
        ]
    })


@app.route('/omfgauth/outgoing/<int:provider_id>/', methods=['POST'])
def omfgauth_outgoing(provider_id):
    provider = current_app.omfgauth_providers[provider_id]
    nonce = 1

    return redirect('{}?nonce={}&for={}'.format(
                                            provider['auth_url'],
                                            nonce,
                                            url_for('.omfgauth_incoming',
                                                    provider_id=provider_id,
                                                    _external=True)))


@app.route('/omfgauth/incoming/<int:provider_id>/')
def omfgauth_incoming(provider_id):
    provider = current_app.omfgauth_providers[provider_id]
    for_ = url_for('.omfgauth_incoming',
                            provider_id=provider_id,
                            _external=True)
    try:
        token = jwt.decode(
                    request.args['token'],
                    provider['secret'],
                    audience=for_,
                    issuer=urlparse(provider['auth_url']).netloc,
                    algorithms=[provider['algorithm']])

    except jwt.exceptions.PyJWTError as exc:
        return jsonify({
            'success': False,
            'reason': str(exc)
        })
    else:
        if 'probe' not in token.get('privileges', []):
            abort(403)
        else:
            session["user"] = token['sub']
            return redirect(url_for('.show_app'))


@app.route('/hooks/<string:name>/', methods=["POST", "GET"])
@auth_required
def webhook(name):
    if request.method == 'POST':
        enqueue(current_app.registry,
                u'webhook:%s' % name,
                request.get_json(True))

        commit()
        return jsonify({"accepted": True})
    else:
        return render_template('webhook.html', name=name, version=version)


@app.route('/api/')
@auth_required
def api_root():
    return jsonify({
        "events": "/api/events/",
        "monitors": "/api/monitors/",
        "login": "/api/login/"
    })


@app.route('/api/login/', methods=["POST"])
def api_login():
    token = current_app.api_tokens.get(request.json.get('api_token', ''))
    if token:
        session["token"] = token
        return jsonify({"success": True})
    else:
        return jsonify({"success": False})


@app.route('/api/events/')
@auth_required
def api_events():
    filter_text = request.args.get('filter', '')
    query_text = request.args.get('query', None)

    since = request.args.get('since', None, type=int)
    until = request.args.get('until', None, type=int)

    if query_text is not None:
        query = json.loads(query_text)
    else:
        query = ['fuzzy', filter_text]

    events = Event.get_by_query(query, since, until)

    return jsonify({
        "events": [format_event(event, queued_count)
                        for event, queued_count in events]
    })


@app.route('/api/views/<viewname>/')
@auth_required
def api_views(viewname):
    view = current_app.views.get(viewname)
    if not view:
        abort(404)

    if current_token() not in view['tokens']:
        abort(403)

    sub_query = json.loads(request.args.get('query', '["all"]'))

    since = request.args.get('since', None, type=int)
    until = request.args.get('until', None, type=int)

    query = ['and', view['query'], sub_query]

    events = Event.get_by_query(query, since, until)

    return jsonify({
        "events": [format_event(event, queued_count)
                        for event, queued_count in events]
    })


def format_event(event, queued_count):
    return {
        "id": event.id,
        "type": event.type,
        "datetime": event.datetime.isoformat(),
        "description": (current_app
                            .registry
                            .get_format(event.type, event.data)),
        "body": event.data,
        "has_queued_executions": queued_count,
    }


@app.route('/api/monitors/')
@auth_required
def api_monitors():
    return jsonify({
        "monitors": [
            {
                "name": name,
                "documentation": doc,
                "triggered_by": {
                    "method": trigger,
                    "type": type,
                },
                "state": MonitorState.get_for_monitor(name),
            }
            for name, doc, trigger, type
            in current_app.registry.get_all_monitors()
        ]
    })


@app.route('/api/monitors/<string:name>/', methods=["PATCH"])
@auth_required
def api_monitor_patch(name):
    MonitorState.set_for_monitor(name, request.json['state'])
    commit()
    return jsonify({"success": True})


@app.route('/api/reports/')
@auth_required
def api_reports():
    return jsonify({
        'reports': [{
            "name": r.name,
            "title": r.title,
        } for r in current_app.registry._reports]
    })


@app.route('/api/reports/<string:name>/')
@auth_required
def api_report(name):
    report = evaluate_report(current_app.registry, name)
    return jsonify(report)
