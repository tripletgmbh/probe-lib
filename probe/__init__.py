# coding: utf-8

from .application import Application
from .monitor import Registry
from .model import db, mig


class Probe(object):

    def __init__(self, config):
        self.registry = Registry()

        self.app = Application(self.registry)
        self.app.update_config(config)

        db.init_app(self.app)
        mig.init_app(self.app)

    def __call__(self, start_response, environ):
        # make it wsgi compliant
        return self.app(start_response, environ)


__all__ = ['Probe']
