from smtplib import SMTP


class Mailer:
    """ Plugin for Mailing via SMTP Server.

    Needs these config values:

    MAIL_SERVER: server by which to send the mail, e.G. 'smtp.example.com'
    MAIL_USER: user with which to identify on the server
    MAIL_PASSWORD: password with which to identify on the server
    MAIL_FROM: email that is given as the sender, e.G. 'noreply@example.com'
    """

    def __init__(self, config, session):
        self.server = config['MAIL_SERVER']
        self.user = config['MAIL_USER']
        self.password = config['MAIL_PASSWORD']
        self.from_ = config['MAIL_FROM']

    def send_mail(self, to, subject, content):
        """ Send a mail

        The mail will be sent via the configured SMTP server, as plaintext.

        to
            Email of the recipient, e.g "alice@example.com"
        subject
            The subject line of the email (string)
        content
            The content of the the email (string)
        """
        message = b'\n'.join([
            b'From: %s',
            b'To: %s',
            b'Subject: %s',
            b'Content-Type: text/plain; charset=utf-8'
            b'',
            b'%s',
        ]) % (
            _to_bytes(self.from_),
            _to_bytes(to),
            _to_bytes(subject),
            _to_bytes(content))

        server = SMTP(self.server)
        server.starttls()
        server.login(self.user, self.password)

        problems = server.sendmail(self.from_, to, message)
        server.quit()

        if problems:
            raise Exception("Errors sending mail: %r" % problems)


def _to_bytes(value):
    if isinstance(value, bytes):
        return bytes
    else:
        return str(value).encode('utf-8')
