""" Add Intervals

Revision ID: 04892cff18ea
Revises: 5edecbbc06f1

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '04892cff18ea'
down_revision = '5edecbbc06f1'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('intervalstate',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('monitor', sa.Unicode(length=50), nullable=False),
        sa.Column('last_triggered', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('monitor')
    )


def downgrade():
    op.drop_table('intervalstate')
