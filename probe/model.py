# coding: utf-8
from pathlib import Path

from sqlalchemy import or_, and_, true, false, exists
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.sql.expression import func

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from datetime import datetime
import json

db = SQLAlchemy()

mig = Migrate(None, db, directory=str(Path(__file__).parent / 'migrations'))


class Base(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    @classmethod
    def get_all(cls):
        return cls.query.all()

    def delete(self):
        db.session.delete(self)


class MonitorState(Base):
    monitor = db.Column(db.Unicode(50), nullable=False, unique=True)
    content = db.Column(db.UnicodeText(), nullable=False, default='{}')

    @classmethod
    def get_for_monitor(cls, monitor):
        state = (cls.query
                    .filter_by(monitor=monitor)
                    .one_or_none())

        return json.loads(str(state.content) if state else '{}')

    @classmethod
    def set_for_monitor(cls, monitor, data):
        state = (cls.query
                    .filter_by(monitor=monitor)
                    .one_or_none())

        if not state:
            state = MonitorState()
            state.monitor = monitor
            db.session.add(state)

        state.content = json.dumps(data)


class MissingSinceTriggerState(Base):
    monitor = db.Column(db.Unicode(50), nullable=False, unique=True)
    last_triggered = db.Column(db.DateTime(), nullable=True, default=None)

    @classmethod
    def set_for_monitor(cls, monitor, last_triggered):
        state = (cls.query
                    .filter_by(monitor=monitor)
                    .one_or_none())

        if not state:
            state = cls()
            state.monitor = monitor
            db.session.add(state)

        state.last_triggered = last_triggered


class IntervalState(Base):
    monitor = db.Column(db.Unicode(50), nullable=False, unique=True)
    last_triggered = db.Column(db.DateTime(), nullable=True, default=None)

    @classmethod
    def set_for_monitor(cls, monitor, last_triggered):
        state = (cls.query
                    .filter_by(monitor=monitor)
                    .one_or_none())

        if not state:
            state = cls()
            state.monitor = monitor
            db.session.add(state)

        state.last_triggered = last_triggered


class Event(Base):
    type = db.Column(db.Unicode(50), nullable=False)
    datetime = db.Column(db.DateTime(), nullable=False, default=datetime.now)
    body = db.Column(db.UnicodeText(), nullable=False)

    def __init__(self, type, data):
        Base.__init__(self)
        self.type = type
        self.data = data
        db.session.add(self)

    def _get_data(self):
        return json.loads(str(self.body))

    def _set_data(self, data):
        self.body = json.dumps(data)

    data = property(_get_data, _set_data)

    @classmethod
    def _build_condition(cls, term):
        type, *args = term
        if type == 'all':
            return true()
        elif type == 'fuzzy':
            return or_(
                Event.type.contains(args[0]),
                Event.body.contains(args[0]))
        elif type == 'attribute':
            name, value = args
            if name == 'type':
                return Event.type == value
            else:
                return false()
        elif type == 'and':
            return and_(*(cls._build_condition(sub) for sub in args))
        elif type == 'or':
            return or_(*(cls._build_condition(sub) for sub in args))
        else:
            raise Exception("unknown type {}".format(type))

    @classmethod
    def get_by_query(cls, query, since, until, offset=0, limit=20):
        q = db.session.query(
                Event,
                exists().where(Event.id == QueuedExecution.event_id))

        if query:
            q = q.filter(cls._build_condition(query))
        if since:
            q = q.filter(Event.id > since)
        if until:
            q = q.filter(Event.id < until)

        return (q
            .order_by(Event.id.desc())
            .offset(offset)
            .limit(limit))

    @classmethod
    def get_last_executions(cls, types):
        return (db.session
                    .query(
                        cls.type,
                        func.max(cls.datetime)
                    )
                    .group_by(cls.type)
                    .filter(cls.type.in_(types))
                    .all())

    @classmethod
    def get_recent_by_types(cls, types, limit):
        return (cls.query
            .filter(cls.type.in_(types))
            .order_by(cls.id.desc())
            .limit(limit))


class QueuedExecution(Base):
    monitor = db.Column(db.Unicode(50), nullable=False)
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))
    event = db.relationship(Event, primaryjoin=(event_id == Event.id))

    def __init__(self, monitor, event):
        self.monitor = monitor
        self.event = event
        db.session.add(self)


def commit():
    db.session.commit()
