# coding: utf-8
import json

from flask import Flask
from jinja2 import StrictUndefined
from werkzeug.middleware.proxy_fix import ProxyFix

from .controller import app as controller_app
from .cli import app as cli_app


class Application(Flask):
    jinja_options = {'undefined': StrictUndefined}

    def __init__(self, registry):
        Flask.__init__(self, __name__)

        self.wsgi_app = ProxyFix(self.wsgi_app)
        self.config.update({
            'PROPAGATE_EXCEPTIONS': True,
            'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        })
        self.registry = registry
        self.register_blueprint(controller_app)
        self.register_blueprint(cli_app)

    @property
    def views(self):
        """ { "viewname": { "query": ["all"], "tokens": ["alice"] } }
        """
        return json.loads(self.config.get('VIEWS', '{}'))

    @property
    def omfgauth_providers(self):
        """
        [{
            "name": "Local Turret",
            "auth_url": "http://localhost:4000/authenticate/",
            "algorithm": "HS256",
            "secret": "EvenMoreSecret",
        }]
        """
        return json.loads(self.config.get('OMFGAUTH_PROVIDERS', '[]'))

    @property
    def api_tokens(self):
        tokens = {}
        if 'API_TOKEN' in self.config:
            tokens[self.config['API_TOKEN']] = "default"
        if 'API_TOKENS' in self.config:
            tokens.update(json.loads(self.config['API_TOKENS']))
        return tokens

    def update_config(self, cfg):
        self.config.update(cfg)

        if 'DEBUG' in cfg:
            self.config['DEBUG'] = cfg['DEBUG'].lower() == 'true'
        if 'DBURI' in cfg:
            self.config['SQLALCHEMY_DATABASE_URI'] = cfg['DBURI']
