import json


def use(config):
    print(json.dumps(config, indent=2))


def config(*args):
    return {
        "version": 1,
        "actions": list(args)
    }


def nanites(*args):
    return {
        "type": "Nanites",
        "steps": list(args)
    }


def step(image, **kwargs):
    return {
        "name": image,
        "arguments": kwargs
    }


def event(type, content):
    return {
        "type": "TriggerEvent",
        "event_type": type,
        "content": content,
    }


import os
import re

env = os.environ

imagename = re.search(r'/([^/]+?)([.]git)?$', env['EVENT_URL']).groups()[0]


use(
    config(
        nanites(
            step(
                "node{}".format(env["EVENT_REPOTYPE"]),
                REPO=env["EVENT_AUTHENTICATED_URL"],
                REVISION=env["EVENT_TAG"]),
            step(
                "analyst",
                PROJECT=imagename,
                VERSION=env["EVENT_TAG"],
                URL=env["EVENT_URL"],
                WEBHOOK=env["PROBE_WEBHOOK_URLTEMPLATE"].replace(
                                                '{name}', 'dependencies')))))
