from setuptools import setup

import os
from itertools import chain

def find_files(relpath):
    prefix = os.path.join(os.path.dirname(__file__), 'probe')
    for dirname, _, files in os.walk(os.path.join(prefix, relpath)):
        for filename in files:
            yield os.path.join(dirname, filename)[len(prefix) + 1:]


setup(
    name='probe-lib',
    version='dev',
    description='Beep? Boop? Beepboop!',
    author='Philipp Benjamin Koeppchen',
    author_email='info@triplet.gmbh',
    url='https://triplet.gmbh/',
    packages=[
        'probe'
    ],
    package_data={
        '': list(chain(
                find_files('static'),
                find_files('migrations'),
                find_files('templates'),
        ))
    },
    install_requires=[
        'Flask==2.0.3',
        'Flask-SQLAlchemy==2.5.1',
        'Flask-Migrate==3.1.0',
        'requests==2.27.1',
        'pyjwt==2.3.0',
    ],
)
